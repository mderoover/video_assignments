# Accessing google.com from your browser
1. Your browser atries to resolve the DNS address
    1. Check browser cache
    2. Check OS cache
    3. Check router cache
    4. Check ISP cache
2. If not found in cache -> ISP DNS server executes DNS query
    1. Contact root domain (.) -> top level domains (com) -> second-level domains (google) -> third level domains (maps)...
3. Browser initiates a TCP request to server (HTTP on top of TCP) (three way handshake)
    1. Client -> \[syn packet\] -> server (open new connection)
    2. Server -> \[syn ack\]    -> client (accept request)
    3. Client -> \[ack\]        -> server (acknoledge syn ack)
    4. TCP connection established!
4. Browser sends HTTP GET request (POST in case you fill in form for example)
    * Additional information included in HTTP header (browser-identification,...)
5. The server handles the request and sends back a HTTP response
    * Webpage you requested (google html code)
    * Status code (200 OK)
6. Browser displays received content from HTTP response
7. Profit!