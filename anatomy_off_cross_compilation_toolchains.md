# Anatomy of cross-compilation toolchains
[Introduction to simple gcc based toolchains](https://www.youtube.com/watch?v=Pbt330zuNPc)

## Cross compilation toolchain
* Build source code into binary for a platform that is different than the one you are building on
 * Three machines involved
    * Build machine (where is it build)
    * Host machine (where does it run) == build machine
    * Target machine (for what machine does it produce code) != build machine
* Toolchain tuple
    * \<arch\>-\<vendor\>-\<os\>-\<libc\/abi\>  
    * \<arm\>-\<foo\>-\<none\>-\<eabi\>
    * OS -> none, linux
* Four main components for Linux cross compilation toolchain
    * binutils
    * gcc
    * Linux kernel headers
    * C library
* Overall build process
    1. Build binutils
    2. Build gcc dependencies
    3. Install Linux kernel headers
    4. Build a first stage gcc: no support for C library, support only for static linking
    5. Build the C library using the first stage gcc
    6. Build the final gcc with C library and support for dynamic linking
* Architecture tuning
    * Gcc can be optimized for arch, cpu, abi, and fpu
* Toolchain VS SDK
    * SDK: toolchain + number of (large) libraries
* Building cross compilation toolchains
    * Provided by distro
    * Crosstool-NG
    * Buildroot
    * Yocto

## Toolchain tuple
### Architecture
### Vendor
### OS
* None
    * baremetal
    * Clib: newlib
    * Cannot buid user space code
    * Can build kernel, bootloader
* Linux
    * Clib: glibc, uclibc, musl
    * Can be used for kernel, bootloader, userspace code
### Libc\/abi
* abi = Application Binary Interface
    * How function calls are made
    * Define calling convention
    * How do functions work with the stack
    * ex. EABI (no assumption on hard float), EABIhf (hard float)

## Four main components for Linux cross compilation toolchain
These component each need to be build
* Binutils
    * Linker ld
    * Assembler as 
    * CPU architecture dependent
* Gcc
    * Is the compiler
    * Provides runtime libraries as well
    * Provide set of headers for standard library
    * dependencies:
* Linux kernel headers
    * Split between user space headers and kernel space headers
    * Describe kernel system calls
    * Kernel to userspace API is always backward compatible (older versions work)
* C library
    * Provides implementation of the POSIX standard functions
    * Glibc: standard but bloated, fully featured
    * UClibc: smaller clib, no API backward compatibility
    * Musl: MIT licensed, small (between glibc and uClibc), fastest

## Multilib toolchain
* Sysroot
    * The logical root directory for headers and libraries
    * Where gcc looks for headers
    * Where ld looks for libraries
    * Parameter passed to binutils and gcc on build
* Most of the time 1 sysroot -> 1 archtecture
* Multilib toolchain
    * Single compiler
    * Multiple sysroots (one per architecture)
    * -> Multiple architectures